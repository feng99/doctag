package de.doctag.docsrv.ui.modals

import de.doctag.docsrv.*
import de.doctag.docsrv.model.FileData
import de.doctag.docsrv.ui.file
import de.doctag.docsrv.ui.modal
import de.doctag.docsrv.ui.pdf
import kweb.*
import kweb.plugins.fomanticUI.fomantic


fun ElementCreator<*>.filePreviewModal(file: FileData) = modal(i18n("ui.modals.filePreviewModal.title","Vorschau")){ modal->
    div(fomantic.ui.placeholder.segment).new{

        when{
            file.contentType.isImage() -> {
                img("/f/${file._id}/download", fomantic.ui.medium.centered.image)
                div(fomantic.ui.divider.hidden)
            }
            file.contentType.isPdf() -> {
                element("iframe", mapOf("style" to "height: 70vh; width:90%; border: none", "src" to "/f/${file._id}/view"))
                div(fomantic.ui.divider.hidden)
            }
            else -> {
                div(fomantic.ui.icon.header).new {
                    i(fomantic.icon.file.pdf.outline)
                    span().i18nText("ui.modals.filePreviewModal.noPreviewAvailable","Keine Vorschau verfügbar")
                }
            }
        }
        div(fomantic.inline).new {
            a(href = "/f/${file._id}/download", attributes = mapOf("download" to "", "class" to "ui button blue")).i18nText("ui.modals.filePreviewModal.downloadButton","Herunterladen")
        }
    }
}

