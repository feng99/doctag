/*
 * DocServer
 * More Info available on https://www.doctag.de
 *
 * The version of the OpenAPI document: 0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.doctag.docsrv_api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for DiscoveryResponse
 */
public class DiscoveryResponseTest {
    private final DiscoveryResponse model = new DiscoveryResponse();

    /**
     * Model tests for DiscoveryResponse
     */
    @Test
    public void testDiscoveryResponse() {
        // TODO: test DiscoveryResponse
    }

    /**
     * Test the property 'identity'
     */
    @Test
    public void identityTest() {
        // TODO: test identity
    }

}
