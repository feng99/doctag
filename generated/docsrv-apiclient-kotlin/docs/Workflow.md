

# Workflow


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**actions** | [**List&lt;WorkflowAction&gt;**](WorkflowAction.md) |  |  [optional]
**name** | **String** |  |  [optional]



