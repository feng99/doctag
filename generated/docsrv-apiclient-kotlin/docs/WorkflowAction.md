

# WorkflowAction


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**List&lt;WorkflowInput&gt;**](WorkflowInput.md) |  |  [optional]
**role** | **String** |  |  [optional]



