

# PublicKeyVerificationResult


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | 
**signature** | **String** |  | 



