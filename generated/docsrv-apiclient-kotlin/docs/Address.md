

# Address


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** |  |  [optional]
**countryCode** | **String** |  |  [optional]
**name1** | **String** |  |  [optional]
**name2** | **String** |  |  [optional]
**street** | **String** |  |  [optional]
**zipCode** | **String** |  |  [optional]



