

# PreparedSignature


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**availableKeys** | [**List&lt;PrivatePublicKeyInfo&gt;**](PrivatePublicKeyInfo.md) |  |  [optional]
**workflow** | [**Workflow**](Workflow.md) |  |  [optional]



