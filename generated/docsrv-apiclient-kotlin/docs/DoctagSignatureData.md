

# DoctagSignatureData


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentHash** | **String** |  |  [optional]
**documentUrl** | **String** |  |  [optional]
**keyFingerprint** | **String** |  |  [optional]
**previousSignaturesHash** | **String** |  |  [optional]
**randomBuffer** | **String** |  |  [optional]
**signature** | **String** |  |  [optional]
**signingDoctagInstance** | **String** |  |  [optional]
**signingUser** | **String** |  |  [optional]
**validFrom** | **String** |  |  [optional]
**validFromDateTime** | **OffsetDateTime** |  |  [optional]
**workflowHash** | **String** |  |  [optional]



