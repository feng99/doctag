

# Person


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]



