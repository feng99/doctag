

# FileData


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**base64Content** | **String** |  |  [optional]
**contentType** | **String** |  |  [optional]
**name** | **String** |  |  [optional]



