

# PublicKeyResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** |  |  [optional]
**owner** | [**Person**](Person.md) |  |  [optional]
**ownerAddress** | [**Address**](Address.md) |  |  [optional]
**publicKey** | **String** |  |  [optional]
**signingDoctagInstance** | **String** |  |  [optional]
**verboseName** | **String** |  |  [optional]
**verification** | [**PublicKeyVerification**](PublicKeyVerification.md) |  |  [optional]



