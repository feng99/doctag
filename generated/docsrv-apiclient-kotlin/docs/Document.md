

# Document


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**attachmentHash** | **String** |  |  [optional]
**attachmentId** | **String** |  |  [optional]
**created** | **OffsetDateTime** |  |  [optional]
**fullText** | **String** |  |  [optional]
**isMirrored** | **Boolean** |  |  [optional]
**mirrors** | **List&lt;String&gt;** |  |  [optional]
**originalFileName** | **String** |  |  [optional]
**signatures** | [**List&lt;Signature&gt;**](Signature.md) |  |  [optional]
**url** | **String** |  |  [optional]
**workflow** | [**Workflow**](Workflow.md) |  |  [optional]



