# docsrv_api
More Info available on https://www.doctag.de

This Dart package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 0.1
- Build package: org.openapitools.codegen.languages.DartClientCodegen
For more information, please visit [https://www.doctag.de](https://www.doctag.de)

## Requirements

Dart 2.0 or later

## Installation & Usage

### Github
If this Dart package is published to Github, add the following dependency to your pubspec.yaml
```
dependencies:
  docsrv_api:
    git: https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```

### Local
To use the package in your local drive, add the following dependency to your pubspec.yaml
```
dependencies:
  docsrv_api:
    path: /path/to/docsrv_api
```

## Tests

TODO

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```dart
import 'package:docsrv_api/api.dart';


final api_instance = DefaultApi();
final documentId = documentId_example; // String | documentId
final hostname = hostname_example; // String | hostname
final embeddedSignature = EmbeddedSignature(); // EmbeddedSignature | 

try {
    final result = api_instance.addSignatureToDoctagDocument(documentId, hostname, embeddedSignature);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->addSignatureToDoctagDocument: $e\n');
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**addSignatureToDoctagDocument**](doc//DefaultApi.md#addsignaturetodoctagdocument) | **POST** /d/{documentId}/{hostname} | Add signature to document
*DefaultApi* | [**checkHealth**](doc//DefaultApi.md#checkhealth) | **GET** /health | Perform Health Check
*DefaultApi* | [**discoverInstance**](doc//DefaultApi.md#discoverinstance) | **GET** /discovery | Perform Instance discovery
*DefaultApi* | [**downloadDocument**](doc//DefaultApi.md#downloaddocument) | **GET** /d/{documentId}/download | Download document
*DefaultApi* | [**downloadFile**](doc//DefaultApi.md#downloadfile) | **GET** /f/{fileId}/download | Perform Instance discovery
*DefaultApi* | [**downloadSignSheet**](doc//DefaultApi.md#downloadsignsheet) | **GET** /d/{documentId}/viewSignSheet | Download sign sheet
*DefaultApi* | [**fetchAuthInfo**](doc//DefaultApi.md#fetchauthinfo) | **GET** /app/auth_info | Check authentication
*DefaultApi* | [**fetchDoctagDocument**](doc//DefaultApi.md#fetchdoctagdocument) | **GET** /d/{documentId} | Fetch doctag document
*DefaultApi* | [**fetchWorkflowToSign**](doc//DefaultApi.md#fetchworkflowtosign) | **GET** /app/signature/prepare/{documentId}/{hostname} | Check authentication
*DefaultApi* | [**notifyChangesOfDoctagDocument**](doc//DefaultApi.md#notifychangesofdoctagdocument) | **POST** /d/notifyChanges/ | Add signature to document
*DefaultApi* | [**setVerificationOfKeyPair**](doc//DefaultApi.md#setverificationofkeypair) | **PUT** /k/{publicKeyFingerprint}/verification | Set the verification of the private public key
*DefaultApi* | [**uploadWorkflowResultAndTriggerSignature**](doc//DefaultApi.md#uploadworkflowresultandtriggersignature) | **POST** /app/signature/push/{documentId}/{hostname} | Check authentication
*DefaultApi* | [**verifyInstanceHasPrivateKey**](doc//DefaultApi.md#verifyinstancehasprivatekey) | **GET** /k/{publicKeyFingerprint}/verify/{seed} | Check that this instance actually owns the given private key
*DefaultApi* | [**viewFile**](doc//DefaultApi.md#viewfile) | **GET** /f/{fileId}/view | Perform Instance discovery


## Documentation For Models

 - [Address](doc//Address.md)
 - [AuthInfoResponse](doc//AuthInfoResponse.md)
 - [DiscoveryResponse](doc//DiscoveryResponse.md)
 - [DoctagSignatureData](doc//DoctagSignatureData.md)
 - [Document](doc//Document.md)
 - [EmbeddedDocument](doc//EmbeddedDocument.md)
 - [EmbeddedSignature](doc//EmbeddedSignature.md)
 - [FileData](doc//FileData.md)
 - [HealthCheckResponse](doc//HealthCheckResponse.md)
 - [NotifyRequest](doc//NotifyRequest.md)
 - [Person](doc//Person.md)
 - [PreparedSignature](doc//PreparedSignature.md)
 - [PrivatePublicKeyInfo](doc//PrivatePublicKeyInfo.md)
 - [PublicKeyResponse](doc//PublicKeyResponse.md)
 - [PublicKeyVerification](doc//PublicKeyVerification.md)
 - [SignInputOptions](doc//SignInputOptions.md)
 - [Signature](doc//Signature.md)
 - [SignatureInputs](doc//SignatureInputs.md)
 - [Workflow](doc//Workflow.md)
 - [WorkflowAction](doc//WorkflowAction.md)
 - [WorkflowInput](doc//WorkflowInput.md)
 - [WorkflowInputOptions](doc//WorkflowInputOptions.md)
 - [WorkflowInputResult](doc//WorkflowInputResult.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author




