# docsrv_api.model.PreparedSignature

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**availableKeys** | [**List<PrivatePublicKeyInfo>**](PrivatePublicKeyInfo.md) |  | [optional] [default to const []]
**workflow** | [**Workflow**](Workflow.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


