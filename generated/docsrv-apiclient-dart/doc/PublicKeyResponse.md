# docsrv_api.model.PublicKeyResponse

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** |  | [optional] 
**owner** | [**Person**](Person.md) |  | [optional] 
**ownerAddress** | [**Address**](Address.md) |  | [optional] 
**publicKey** | **String** |  | [optional] 
**signingDoctagInstance** | **String** |  | [optional] 
**verboseName** | **String** |  | [optional] 
**verification** | [**PublicKeyVerification**](PublicKeyVerification.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


