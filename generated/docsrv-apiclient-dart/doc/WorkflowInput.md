# docsrv_api.model.WorkflowInput

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**kind** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**options** | [**WorkflowInputOptions**](WorkflowInputOptions.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


