# docsrv_api.model.Address

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** |  | [optional] 
**countryCode** | **String** |  | [optional] 
**name1** | **String** |  | [optional] 
**name2** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**zipCode** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


