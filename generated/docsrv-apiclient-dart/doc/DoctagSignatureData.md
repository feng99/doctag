# docsrv_api.model.DoctagSignatureData

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentHash** | **String** |  | [optional] 
**documentUrl** | **String** |  | [optional] 
**keyFingerprint** | **String** |  | [optional] 
**previousSignaturesHash** | **String** |  | [optional] 
**randomBuffer** | **String** |  | [optional] 
**signature** | **String** |  | [optional] 
**signingDoctagInstance** | **String** |  | [optional] 
**signingUser** | **String** |  | [optional] 
**validFrom** | **String** |  | [optional] 
**validFromDateTime** | [**DateTime**](DateTime.md) |  | [optional] 
**workflowHash** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


